﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PawaoxUtility.Metro
{
    public class LoadingPopupViewModel : INotifyPropertyChanged
    {
        public LoadingPopupViewModel(double width, double height)
        {
            Width = width;
            Height = height;
            SpeedRatio = 0.6;
            LoadingColor = Brushes.Blue;
        }

        double _speedRatio;
        public double SpeedRatio
        {
            get { return _speedRatio; }
            set
            {
                if (_speedRatio != value)
                {
                    _speedRatio = value;
                    OnPropertyChanged("SpeedRatio");
                }
            }
        }

        double _width;
        public double Width
        {
            get { return _width; }
            set
            {
                if (_width != value)
                {
                    _width = value;
                    OnPropertyChanged("Width");
                }
            }
        }

        double _height;
        public double Height
        {
            get { return _height; }
            set
            {
                if (_height != value)
                {
                    _height = value;
                    OnPropertyChanged("Height");
                }
            }
        }

        Brush _foreground;
        public Brush Foreground
        {
            get { return _foreground; }
            set
            {
                if (_foreground != value)
                {
                    _foreground = value;
                    OnPropertyChanged("Foreground");
                }
            }
        }

        Brush _loadingColor;
        public Brush LoadingColor
        {
            get { return _loadingColor; }
            set
            {
                if (_loadingColor != value)
                {
                    _loadingColor = value;
                    OnPropertyChanged("LoadingColor");
                }
            }
        }

        Brush _background;
        public Brush Background
        {
            get { return _background; }
            set
            {
                if (_background != value)
                {
                    _background = value;
                    OnPropertyChanged("Background");
                }
            }
        }

        string _processText;
        public string ProcessText
        {
            get { return _processText; }
            set
            {
                if (_processText == null || !_processText.Equals(value))
                {
                    _processText = value;
                    OnPropertyChanged("ProcessText");
                }
            }
        }
        
        #region Interface Implementations
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // Protected
        protected void OnPropertyChanged(string propertyname)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        #endregion
        #endregion
    }
}
