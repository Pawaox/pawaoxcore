﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility.Loaders
{
    public class KeyLoadPair
    {
        public static HashSet<string> UsedKeys;

        public string Key { get; set; }
        public string LoadData { get; set; }

        public KeyLoadPair(string key, string loadData)
        {
            if (UsedKeys != null)
            {
                if (UsedKeys.Contains(key))
                    throw new ArgumentException("KeyLoadPair constructor> Key '" + key + "' is already in use! Bad boy!");
                UsedKeys.Add(key);
            }
            else UsedKeys = new HashSet<string>();

            this.Key = key;
            this.LoadData = loadData;
        }

        public static bool IsKeyAvailable(string key)
        {
            if (UsedKeys != null)
                return !UsedKeys.Contains(key);

            UsedKeys = new HashSet<string>();
            return true;
        }
    }
}
