﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility
{
    public static class PawFileHandler
    {
        #region IO Management
        public static void CreateFile(string location, bool overrideIfExists = false)
        {
            if (overrideIfExists || !File.Exists(location))
                using (var stream = File.Open(location, FileMode.Create)) { }
        }

        public static void CreateDirectories(string path, bool overrideIfExists = false)
        {
            if (overrideIfExists || !Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public static bool ExistsDir(string path)
        {
            return Directory.Exists(path);
        }

        public static bool ExistsFile(string path)
        {
            return File.Exists(path);
        }
        #endregion

        #region Writing
        public static void WriteBinary(string location, byte[] bytes)
        {
            using (Stream stream = File.Open(location, FileMode.Create))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    try
                    {
                        writer.Write(bytes);
                    }
                    finally
                    {
                        stream.Close();
                        writer.Close();
                    }
                }
            }
        }

        public static void Write(string location, object objectToWrite)
        {
            using (Stream stream = File.Open(location, FileMode.Create))
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, objectToWrite);

                stream.Close();
            }
        }

        public static void WritePlaintext(string location, string text)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(location))
                writer.WriteLine(text);
        }

        public static void Append<T>(string location, T objectToWrite)
        {
            using (Stream stream = File.Open(location, FileMode.Append))
            {
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, objectToWrite);
                stream.Close();
            }
        }

        public static string ReadPlaintext(string location)
        {
            return File.ReadAllText(location, Encoding.ASCII);
                /*
            File.ReadAllText(plocation, Encoding.Default);
            string result = "";

            if (File.Exists(location))
                using (Stream stream = File.Open(location, FileMode.Open))
                    result = new StreamReader(stream).ReadToEnd();

            return result;*/
        }

        public static T ReadSerialized<T>(string location)
        {
            T element = default(T);

            try
            {
                using (Stream stream = File.Open(location, FileMode.Open))
                {
                    BinaryFormatter bFormatter = new BinaryFormatter();
                    element = (T)bFormatter.Deserialize(stream);
                    stream.Close();
                }
            }
            catch (IOException ie)
            {
                Console.Write("FileHandler.write EXCEPTION: ");
                Console.Write(ie.Message + "\n---\n");
            }

            return element;
        }
        #endregion
    }
}
