﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility
{
    public class SQLBuilder
    {
        public string AttributePrefix = "";

        string SQL = "";

        public SQLBuilder() { }
        public SQLBuilder(string initial) { Add(initial); }
        public SQLBuilder(string initial, params object[] objects) { Add(initial, objects); }

        public SQLBuilder Add(string sql)
        {
            if (SQL.Length != 0)
                SQL += " ";
            SQL += sql;

            return this;
        }

        string ObjectParamsReplace(string sql, string toReplace, params object[] objects)
        {
            for (int i = 0; i < objects.Length; i++)
                sql = sql.Replace(toReplace + i, objects[i].ToString());

            return sql;
        }

        public SQLBuilder Add(string sql, params object[] objects)
        {
            sql = ObjectParamsReplace(sql, "#", objects);
            Add(sql);

            return this;
        }

        #region Select/From/Where

        public SQLBuilder SelectAll()
        {
            Add("SELECT *");

            return this;
        }

        public SQLBuilder Select(string sql)
        {
            Add("SELECT " + sql);

            return this;
        }

        public SQLBuilder Select(string sql, params object[] objects)
        {
            Add("SELECT " + sql, objects);

            return this;
        }

        public SQLBuilder From(string sql)
        {
            Add("FROM " + sql);

            return this;
        }

        public SQLBuilder From(string sql, params object[] objects)
        {
            Add("FROM " + sql, objects);

            return this;
        }

        public SQLBuilder Where(string sql)
        {
            Add("WHERE " + sql);

            return this;
        }

        public SQLBuilder Where(string sql, params object[] objects)
        {
            Add("WHERE " + sql, objects);

            return this;
        }

        public string SelectPrefix(string prefix, string columnInitials, params string[] columns)
        {
            prefix += "_";
            columnInitials += ".";

            if (!SQL.Contains("SELECT"))
                Add("SELECT");
            else SQL += ",";

            for (int i = 0; i < columns.Length; i++)
            {
                string column = columns[i];
                if (i != 0)
                    SQL += ",";
                string full = columnInitials + column + " AS '" + columnInitials + prefix + column + "'";
                Add(full);
            }

            return columnInitials + prefix;
        }

        public string SelectPrefixTOP(int topAmount, string prefix, string columnInitials, params string[] columns)
        {
            prefix += "_";
            columnInitials += ".";

            if (!SQL.Contains("SELECT"))
                Add("SELECT TOP " + topAmount);
            else SQL += ",";

            for (int i = 0; i < columns.Length; i++)
            {
                string column = columns[i];
                if (i != 0)
                    SQL += ",";
                string full = columnInitials + column + " AS '" + columnInitials + prefix + column + "'";
                Add(full);
            }

            return columnInitials + prefix;
        }

        #endregion

        #region Separators (And, Or etc)

        public SQLBuilder Or(string afterOr = "")
        {
            if (afterOr.Length > 0)
                Add("OR " + afterOr);
            else Add("OR");

            return this;
        }

        public SQLBuilder And(string afterAnd = "")
        {
            if (afterAnd.Length > 0)
                Add("AND " + afterAnd);
            else Add("AND");

            return this;
        }

        public SQLBuilder Values(string sql)
        {
            Add("VALUES (" + sql + ")");
            return this;
        }

        public SQLBuilder Set(string afterSet = "")
        {
            if (afterSet.Length > 0)
                Add("SET " + afterSet);
            else Add("SET");

            return this;
        }

        #endregion

        #region Like

        public SQLBuilder WhereLike(string attrib, string search)
        {
            Add("WHERE " + AttributePrefix + attrib + " LIKE " + search);

            return this;
        }

        public SQLBuilder OrLike(string attrib, string search)
        {
            return Or().Like(attrib, search);
        }

        public SQLBuilder Like(string attrib, string search)
        {
            Add(AttributePrefix + attrib + " LIKE " + search);

            return this;
        }

        #endregion

        public string Finish()
        {
            if (!SQL.EndsWith(";"))
                SQL += ";";

            return SQL;
        }

        public override string ToString()
        {
            return SQL;
        }
    }
}
