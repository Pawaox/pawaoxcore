﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PawaoxUtility
{
    public static class PawStopwatchHelper
    {
        public static string ToStringShort(Stopwatch watch)
        {
            return "";
        }

        public static string ToStringFull(Stopwatch watch)
        {
            return "";
        }

        public static Stopwatch StartNew()
        {
            return Stopwatch.StartNew();
        }


        #region Helper Methods

        public static long TimeInSeconds(Stopwatch watch)
        {
            return watch.ElapsedMilliseconds / 1000;
        }
        public static long TimeInMinutes(Stopwatch watch)
        {
            return TimeInSeconds(watch) / 60;
        }
        public static long TimeInHours(Stopwatch watch)
        {
            return TimeInHours(watch) / 60;
        }
        public static long TimeInDays(Stopwatch watch)
        {
            return TimeInHours(watch) / 24;
        }

        #endregion

    }
}
