﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility
{
    public static class PawWebHandler
    {
        #region Downloading
        public static byte[] DownloadImageFromURL(string url)
        {
            byte[] result;

            HttpWebRequest imageRequest = (HttpWebRequest)WebRequest.Create(url);

            using (WebResponse imageWebResponse = imageRequest.GetResponse())
            {
                using (System.IO.Stream imageStream = imageWebResponse.GetResponseStream())
                {
                    result = PawConverter.StreamToByteArray(imageStream);
                }
            }

            return result;
        }
        #endregion

        #region Website Source
        public static byte[] DownloadWebsiteSourceRaw(string url)
        {
            using (var client = new WebClient())
            {
                return client.DownloadData(url);
            }
        }

        public static string DownloadWebsiteSource(String url)
        {
            return System.Text.Encoding.UTF8.GetString(DownloadWebsiteSourceRaw(url));
        }
        #endregion

        #region Link Cleanup

        public static string RemoveClutterAtStartOfLink(string url)
        {
            url = url.ToLower();

            if (url.StartsWith("http://") || url.StartsWith("http:\\\\"))
                url = url.Substring(7);
            else if (url.StartsWith("https://") || url.StartsWith("https:\\\\"))
                url = url.Substring(8);

            if (url.StartsWith("www."))
                url = url.Substring(4);

            return url;
        }
        #endregion
    }
}
