﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PawaoxUtility.Metro
{
    public partial class LoadingPopupWindow : MetroWindow
    {
        public Thread MyThread { get; set; }

        public LoadingPopupViewModel ViewModel
        {
            get { return DataContext as LoadingPopupViewModel; }
            set { DataContext = value; }
        }

        public LoadingPopupWindow()
        {
            ViewModel = new LoadingPopupViewModel(150, 200);

            InitializeComponent();
        }

        public LoadingPopupWindow(LoadingPopupViewModel viewModel)
        {
            ViewModel = viewModel;

            InitializeComponent();
        }
    }
}
