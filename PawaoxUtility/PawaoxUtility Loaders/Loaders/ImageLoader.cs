﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PawaoxUtility.Loaders
{
    public class ImageLoader : FileLoader<ImageSource>
    {
        public ImageLoader(string defaultFolder, string defaultFileType) : base(defaultFolder, defaultFileType) { }

        protected override void DoLoad(Assembly assembly, object groupKey, KeyLoadPair pair)
        {
            //ImageSource image = PawResourceHandler.GetImageFromResource(assembly, pair.LoadData);
            ImageSource image = PawResourceHandler.GetImageSourceFromResource(assembly, pair.LoadData);
            files[groupKey][pair.Key] = image;
        }
    }
}
