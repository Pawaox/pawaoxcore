﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PawaoxUtility.Loaders
{
    public abstract class FileLoader<T>
    {
        public bool PreparationFinished { get; set; }

        string _defaultFolder;
        public string DefaultFolder
        {
            get { return _defaultFolder; }
            set
            {
                _defaultFolder = value;
                if (!_defaultFolder.EndsWith("/"))
                    _defaultFolder += "/";
            }
        }

        string _defaultFileType;
        public string DefaultFileType
        {
            get { return _defaultFileType; }
            set
            {
                _defaultFileType = value;
                if (!_defaultFileType.StartsWith("."))
                    _defaultFileType += ".";
            }
        }

        protected Dictionary<object, List<KeyLoadPair>> fileGroups;
        protected Dictionary<object, Dictionary<string, T>> files;

        FileLoader() { }
        protected FileLoader(string defaultFolder, string defaultFileType)
        {
            PreparationFinished = false;

            this.DefaultFolder = defaultFolder;
            this.DefaultFileType = defaultFileType;

            fileGroups = new Dictionary<object, List<KeyLoadPair>>();
            files = new Dictionary<object, Dictionary<string, T>>();
        }

        public T Get(object groupKey, string imageKey)
        {
            if (fileGroups.ContainsKey(groupKey))
                if (files.ContainsKey(imageKey))
                    return files[groupKey][imageKey];

            return default(T);
        }

        public void Load(Assembly assembly, object groupKey)
        {
            if (!fileGroups.ContainsKey(groupKey))
                throw new ArgumentException("FileLoader.Load (PreLoad Method)> groupKey '" + groupKey + "' doesn't exist...");

            if (!files.ContainsKey(groupKey))
                files.Add(groupKey, new Dictionary<string, T>());

            foreach (var pair in fileGroups[groupKey])
                DoLoad(assembly, groupKey, pair);
        }

        abstract protected void DoLoad(Assembly assembly, object groupKey, KeyLoadPair pair);

        public void LoadEverything(Assembly assembly)
        {
            foreach (object groupKey in fileGroups.Keys)
                Load(assembly, groupKey);
        }

        public void Unload(object groupKey)
        {
            if (!fileGroups.ContainsKey(groupKey))
                throw new ArgumentException("ImageLoader.UnloadImages> groupKey '" + groupKey + "' doesn't exist...");

            foreach (var pair in fileGroups[groupKey])
                files[groupKey][pair.Key] = default(T);
            GC.Collect();
        }
        public void UnloadEverything()
        {
            foreach (object groupKey in fileGroups.Keys)
            {
                foreach (KeyLoadPair pair in fileGroups[groupKey])
                    files[groupKey][pair.Key] = default(T);

                files[groupKey] = null;
            }
            GC.Collect();
        }

        public void Add(object groupKey, string imageKey, string imageName)
        {
            if (!KeyLoadPair.IsKeyAvailable(imageKey))
                throw new ArgumentException("ImageLoader.Add> imageKey '" + imageKey + "' is already used...");

            AddFullPath(groupKey, imageKey, DefaultFolder + imageName + DefaultFileType);
        }

        public void Add(object groupKey, string imageKey, string imageFullName, string imageFolderPath = "")
        {
            if (imageFolderPath == null ||imageFolderPath.Length == 0)
                imageFolderPath = DefaultFolder;
            else if (imageFolderPath.EndsWith("/"))
                imageFolderPath += "/";

            AddFullPath(groupKey, imageKey, imageFolderPath + imageFullName);
        }

        public void AddFullPath(object groupKey, string imageKey, string fullImagePathAndName)
        {
            if (!KeyLoadPair.IsKeyAvailable(imageKey))
                throw new ArgumentException("ImageLoader.Add> imageKey '" + imageKey + "' is already used...");

            if (!fileGroups.ContainsKey(groupKey))
                fileGroups.Add(groupKey, new List<KeyLoadPair>());

            fileGroups[groupKey].Add(new KeyLoadPair(imageKey, fullImagePathAndName));
        }
    }
}
