﻿using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PawaoxUtility.Metro
{
    public class PawThemeManager
    {
        public PawThemeManager() { SetDefault(); }
        public PawThemeManager(ThemeLayout layout, ThemeAccent accent)
        {
            ChangeTheme(layout, accent);
        }

        public ThemeLayout CurrentLayout { get; private set; }
        public ThemeAccent CurrentAccent { get; private set; }

        public void ChangeTheme(ThemeLayout layout, ThemeAccent accent)
        {
            ThemeManager.ChangeAppStyle(Application.Current,
                ThemeManager.GetAccent(AccentToString(accent)),
                ThemeManager.GetAppTheme(LayoutToString(layout))
            );

            CurrentLayout = layout;
            CurrentAccent = accent;
        }

        public void SetDefault()
        {
            ChangeTheme(ThemeLayout.LIGHT, ThemeAccent.COBALT);
        }


        public void AddLayout(string name, Uri uri)
        {
            throw new NotImplementedException("PawThemeManager.AddLayout");
            //ThemeManager.AddLayout()
        }
        public void AddAccent(string name, Uri uri)
        {
            throw new NotImplementedException("PawThemeManager.AddAccent");
            //ThemeManager.AddAccent()
        }

        private string LayoutToString(ThemeLayout layout)
        {
            string result = "";

            switch (layout)
            {
                case ThemeLayout.DARK:
                    result = "BaseDark";
                    break;
                case ThemeLayout.LIGHT:
                    result = "BaseLight";
                    break;
            }

            return result;
        }

        private string AccentToString(ThemeAccent accent)
        {
            return accent.ToString();
        }

    }

    public enum ThemeAccent
    {
        RED, GREEN, BLUE, PURPLE, ORANGE, LIME, EMERALD, TEAL, CYAN, COBALT, INDIGO, VIOLET, PINK, MAGENTA, CRIMSON, AMBER, YELLOW, BROWN, OLIVE, STEEL, MAUVE, TAUPE, SIENNA
    }

    public enum ThemeLayout
    {
        DARK, LIGHT
    }
}
