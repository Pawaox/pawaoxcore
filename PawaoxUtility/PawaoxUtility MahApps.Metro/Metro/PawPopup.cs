﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PawaoxUtility.Metro
{
    public static class PawPopup
    {
        public static void Show(Window owner, LoadingPopupViewModel viewModel, Action<IProgress<string>> codeToRun)
        {
            if (owner == null)
                throw new ArgumentNullException("PawPopup.Show > parameter 'owner' of type 'Window' cannot be null.");
            if (viewModel == null)
                throw new ArgumentNullException("PawPopup.Show > parameter 'viewModel' of type 'LoadingPopupViewModel' cannot be null.");

            Show(owner, new LoadingPopupWindow(viewModel), codeToRun);
        }
        public static void Show(Window owner, double width, double height, Action<IProgress<string>> codeToRun)
        {
            if (owner == null)
                throw new ArgumentNullException("PawPopup.Show > parameter 'owner' of type 'Window' cannot be null.");

            Show(owner, new LoadingPopupWindow(new LoadingPopupViewModel(width, height)), codeToRun);
        }

        static void Show(Window owner, LoadingPopupWindow window, Action<IProgress<string>> codeToRun)
        {
            var thread = new Thread(() =>
            {
                window.MyThread = Thread.CurrentThread;

                window.Closed += (senda, args) => window.Dispatcher.InvokeShutdown();

                window.Loaded += (senda, args) =>
                {
                    BackgroundWorker worker = new BackgroundWorker();

                    Progress<string> progress = new Progress<string>(data => window.ViewModel.ProcessText = data);

                    worker.DoWork += (s, workerArgs) =>
                    {
                        codeToRun(progress);
                    };

                    worker.RunWorkerCompleted += (s, workerArgs) => { window.Close(); };

                    worker.RunWorkerAsync();
                };

                window.Dispatcher.InvokeAsync(() =>
                {
                    System.Windows.Threading.Dispatcher.Run();
                });
                window.ShowDialog();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }
    }
}
