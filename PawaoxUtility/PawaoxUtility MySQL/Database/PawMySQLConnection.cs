﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PawaoxUtility.Database
{
    public class PawMySQLConnection : IDisposable
    {
        private readonly string connection;

        private MySqlConnection con;

        public PawMySQLConnection(string serverAddress, string database, string username, string password)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Server=" + serverAddress + ";");
            builder.Append("Database=" + database + ";");
            builder.Append("Uid=" + username + ";");
            builder.Append("Pwd=" + password + ";");

            con = new MySqlConnection(connection = builder.ToString());
        }

        public PawMySQLConnection(string connectionString)
        {
            con = new MySqlConnection(connection = connectionString);
        }

        ~PawMySQLConnection()
        {
            try { Close(); }
            catch(Exception) { }
            finally { try { con.Dispose(); } catch (Exception) { } }
        }

        #region [WIP] Transactions

#warning TODO: Store pointers to transactions. In deconstructor, attempt to close them all in loop to avoid possible loss of data. Prevents the 'horrorstories' of losing a full days work.
#warning TODO: Methods in file close the connection, messing with transaction use. Create new file dedicated to transactions, method below temporary.

        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public MySqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.Unspecified)
        {
            Open();
            return con.BeginTransaction(isolation);
        }

        #endregion

        #region Query
        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public MySqlDataReader Query(string sql)
        {
            return Query(CreateCommand(sql));
        }


        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public MySqlDataReader Query(MySqlCommand cmd)
        {
            Open();

            MySqlException exception = null;
            MySqlDataReader rdr = null;

            try { rdr = cmd.ExecuteReader(); }
            catch (MySqlException e) { exception = e; }
            finally
            {
                if (exception != null)
                {
                    Close();
                    throw exception;
                }
            }

            return rdr;
        }

        public List<T> QuerySimple<T>(int columns, string sql)
        {
            MySqlCommand cmd = CreateCommand(sql);

            Open();

            MySqlException exception = null;
            MySqlDataReader rdr = null;
            
            try { rdr = cmd.ExecuteReader(); }
            catch (MySqlException e) { exception = e; }
            finally
            {
                if (exception != null)
                {
                    Close();
                    throw exception;
                }
            }

            List<T> list = new List<T>();

            if (rdr != null)
                while (rdr.Read())
                    for (int i = 0; i < columns; i++)
                        list.Add((T)rdr[i]);

            Close();

            return list;
        }

        #endregion

        #region Selects

        public T SelectScalar<T>(string sql)
        {
            return SelectScalar<T>(CreateCommand(sql));
        }

        public T SelectScalar<T>(MySqlCommand cmd)
        {
            Open();

            var fetch = cmd.ExecuteScalar();
            T t = (T)fetch;

            Close();

            return t;
        }

        #endregion

        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public MySqlDataReader StoredProcedureReader(string procedure, params PawMySQLStatementParam[] parameters)
        {
            MySqlCommand cmd = CreateStoredProcedure(procedure, parameters);

            return Query(cmd);
        }

        public int PreparedStatementExecute(string sql, params PawMySQLStatementParam[] parameters)
        {
            return Execute(CreatePreparedStatement(sql, false, parameters));
        }

        public MySqlCommand CreatePreparedStatement(string sql, bool procedure, params PawMySQLStatementParam[] parameters)
        {
            MySqlCommand cmd = new MySqlCommand(sql, con);

            if (procedure)
                cmd.CommandType = CommandType.StoredProcedure;

            foreach (var param in parameters)
                cmd.Parameters.Add(param.MySQLParam);

            Open();
            cmd.Prepare();
            Close();

            return cmd;
        }

        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public MySqlDataReader PreparedStatementQuery(string sql, params PawMySQLStatementParam[] parameters)
        {
            return Query(CreatePreparedStatement(sql, false, parameters));
        }

        public int PreparedStatement(string sql, params PawMySQLStatementParam[] parameters)
        {
            MySqlCommand cmd = CreatePreparedStatement(sql, false, parameters);
            return Execute(cmd);
        }

        public T PreparedStatementScalar<T>(string sql, params PawMySQLStatementParam[] parameters)
        {
            MySqlCommand cmd = CreatePreparedStatement(sql, false, parameters);
            return SelectScalar<T>(cmd);
        }

        public decimal? PreparedStatementGetIdentity(string sql, params PawMySQLStatementParam[] parameters)
        {
            MySqlCommand cmd = CreatePreparedStatement(sql + "; SELECT Scope_Identity();", false, parameters);

            return SelectScalar<decimal?>(cmd);
        }

        public MySqlCommand CreateStoredProcedure(string procedure, params PawMySQLStatementParam[] parameters)
        {
            return CreatePreparedStatement(procedure, true, parameters);
        }


        SqlParameter CreateMySqlParameter(string variable, SqlDbType type, int varCharLength = 0)
        {
            if (type == SqlDbType.VarChar)
                return new SqlParameter(variable, type, varCharLength);
            else return new SqlParameter(variable, type);
        }





        public void Open()
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
        }

        public void Close()
        {
            try
            {
                if (con.State == ConnectionState.Open && con.State != ConnectionState.Closed)
                    con.Close();
            }
            //It happened too fast, doesn't matter!
            catch (InvalidOperationException) { }
        }

        public int Execute(string sql)
        {
            return Execute(CreateCommand(sql));
        }

        public int Execute(MySqlCommand cmd)
        {
            Open();
            int rows = cmd.ExecuteNonQuery();
            Close();

            return rows;
        }

        public MySqlCommand CreateCommand(string sql)
        {
            return new MySqlCommand(sql, con);
        }

        public void Dispose()
        {
            con.Dispose();
        }
    }
}
