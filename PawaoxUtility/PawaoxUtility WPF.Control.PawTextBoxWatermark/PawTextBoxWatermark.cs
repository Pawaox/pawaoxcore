﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PawaoxUtility.WPF.Control
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:PawaoxUtility.WPF.Control"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:PawaoxUtility.WPF.Control;assembly=PawaoxUtility WPF.Control.PawTextBoxWatermark"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:PawTextBoxWatermark/>
    ///
    /// </summary>
    public class PawTextBoxWatermark : TextBox, INotifyPropertyChanged
    {
        static PawTextBoxWatermark()
        {
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(PawTextBoxWatermark), new FrameworkPropertyMetadata(typeof(PawTextBoxWatermark)));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void WatermarkEnabledHandler();
        public event WatermarkEnabledHandler WatermarkEnabled;

        public delegate void WatermarkDisabledHandler();
        public event WatermarkDisabledHandler WatermarkDisabled;


        Brush _foregroundWatermark;
        public Brush ForegroundWatermark
        {
            get { return _foregroundWatermark; }
            set
            {
                if (_foregroundWatermark != value)
                {
                    _foregroundWatermark = value;
                    OnPropertyChanged("ForegroundWatermark");
                }
            } 
        }

        Brush _foregroundDefault;
        public Brush ForegroundDefault
        {
            get { return _foregroundDefault; }
            set
            {
                if (_foregroundDefault != value)
                {
                    _foregroundDefault = value;
                    OnPropertyChanged("ForegroundDefault");
                }
            }
        }

        public bool IsWatermarkVisible
        {
            get { return watermarkVisible; }
        }

        public string TrueText
        {
            get
            {
                if (watermarkVisible)
                    return "";
                else return Text;
            }
        }

        public string Watermark { get; set; }


        bool watermarkVisible;

        public PawTextBoxWatermark() : this("WATERMARK") {}

        public PawTextBoxWatermark(string watermark)
        {
            ForegroundDefault = TextElement.GetForeground(this);

            GotFocus += OnFocusGet;
            LostFocus += OnFocusLost;
            Loaded += OnLoaded;
        }

        void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #region EventHandlers
        void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (Text == null || Text.Trim().Length == 0)
                SetWatermark();
        }

        void OnFocusGet(object sender, RoutedEventArgs e)
        {
            if (Watermark.Equals(Text) && ForegroundWatermark.Equals(this.Foreground))
                RemoveWatermark();
        }

        void OnFocusLost(object sender, RoutedEventArgs e)
        {
            if (Text == null || Text.Trim().Length == 0)
                SetWatermark();
        }
        #endregion

        #region Event Helpers
        public void RemoveWatermark()
        {
            watermarkVisible = false;
            this.Text = "";
            this.Foreground = ForegroundDefault;

            if (WatermarkDisabled != null)
                WatermarkDisabled();
        }

        public void SetWatermark()
        {
            watermarkVisible = true;
            this.Text = Watermark;
            this.Foreground = ForegroundWatermark;

            if (WatermarkEnabled != null)
                WatermarkEnabled();
        }
        #endregion
    }
}
