﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility
{
    public static class PawMath
    {
        #region Rounding
        public static double RoundDecimals(double number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            double temp = Math.Round(number * times);

            return temp / times;
        }

        public static double RoundDecimalsUp(double number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            return (Math.Ceiling(number * times) / times);
        }

        public static double RoundDecimalsDown(double number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            return (Math.Floor(number * times) / times);
        }

        public static double RoundDecimals(float number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            double temp = Math.Round(number * times);

            return temp / times;
        }

        public static double RoundDecimalsUp(float number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            return (Math.Ceiling(number * times) / times);
        }

        public static double RoundDecimalsDown(float number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            return (Math.Floor(number * times) / times);
        }

        public static decimal RoundDecimals(decimal number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            decimal temp = Decimal.Round(number * times);

            return temp / times;
        }

        public static decimal RoundDecimalsUp(decimal number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            return (Math.Ceiling(number * times) / times);
        }

        public static decimal RoundDecimalsDown(decimal number, int decimals)
        {
            int times = (int)Math.Pow(10, decimals);
            return (Math.Floor(number * times) / times);
        }
        #endregion
    }
}
