﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility.Database
{
    public class PawSqlStatementParam
    {
        public SqlDbType Type { get; set; }
        public string Variable { get; set; }
        public object Value { get; set; }
        public int TypeNumber { get; set; }

        public SqlParameter SQLParam
        {
            get
            {
                SqlParameter param = null;

                if (Type == SqlDbType.VarChar)
                    param = new SqlParameter(Variable, Type, TypeNumber);
                else param = new SqlParameter(Variable, Type);
                param.Value = Value;

                return param;
            }

            private set
            {
            }
        }

        public PawSqlStatementParam(SqlDbType type, string variable, object value, int typeLength = 0)
        {
            Type = type;
            Variable = variable;
            Value = value;
            TypeNumber = typeLength;
        }
    }
}
