﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility.Database
{
    public class PawMySQLStatementParam
    {
        public MySqlDbType Type { get; set; }
        public string Variable { get; set; }
        public object Value { get; set; }
        public int TypeNumber { get; set; }

        public MySqlParameter MySQLParam
        {
            get
            {
                MySqlParameter param = null;
                
                if (Type == MySqlDbType.VarChar)
                    param = new MySqlParameter(Variable, Type, TypeNumber);
                else param = new MySqlParameter(Variable, Type);
                param.Value = Value;

                return param;
            }

            private set { }
        }

        public PawMySQLStatementParam(MySqlDbType type, string variable, object value, int typeLength = 0)
        {
            Type = type;
            Variable = variable;
            Value = value;
            TypeNumber = typeLength;
        }
    }
}
