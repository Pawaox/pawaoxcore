﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility.Database
{
    public class PawMySQLDataReader : IDisposable
    {
        MySql.Data.MySqlClient.MySqlDataReader Reader { get; set; }

        public string KeyPrefix { get; set; }
        public string KeySuffix { get; set; }

        public PawMySQLDataReader(MySql.Data.MySqlClient.MySqlDataReader reader)
        {
            KeyPrefix = "";
            KeySuffix = "";
            this.Reader = reader;
        }

        public T Fetch<T>(string key, bool ignorePrefix = false, bool ignoreSuffix = false, T nullReturnValue = default(T))
        {
            object obj = Reader[(ignorePrefix ? "" : KeyPrefix) + key + (ignoreSuffix ? "" : KeySuffix)];

            //T result = obj is DBNull ? nullReturnValue : (obj == null ? nullReturnValue : (T)obj);
            T result = obj is DBNull ? nullReturnValue : (obj is Nullable && obj == null ? nullReturnValue : (T)obj);

            obj = null;

            return result;
        }

        public string Fetch(string key, bool ignorePrefix = false, bool ignoreSuffix = false, string nullReturnValue = "")
        {
            object obj = Reader[(ignorePrefix ? "" : KeyPrefix) + key + (ignoreSuffix ? "" : KeySuffix)];

            string result = obj is DBNull ? nullReturnValue : (obj == null ? nullReturnValue : obj.ToString());
            obj = null;

            return result;
        }

        public bool FetchBool(string key)
        {
            bool result = Reader.GetBoolean(key);
            return result;
        }

        public string FetchDateTime(string key)
        {
            string result = "";
            var time = Fetch<DateTime?>(key);

            if (time.HasValue)
                result = time.Value.ToString();

            return result;
        }

        public string FetchDate(string key)
        {
            string result = "";
            var time = Fetch<DateTime?>(key);

            if (time.HasValue)
                result = time.Value.ToString("yyyy-MM-dd");

            return result;
        }

        public bool NextResult()
        {
            return Reader.NextResult();
        }

        public bool Read()
        {
            return Reader.Read();
        }

        public void Dispose()
        {
            try { Reader.Dispose(); }
            catch (Exception) { }
        }
    }
}
