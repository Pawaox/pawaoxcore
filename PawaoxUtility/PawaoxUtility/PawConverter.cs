﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility
{
    public static class PawConverter
    {
        #region Converters
        public static byte[] StreamToByteArray(Stream stream)
        {
            byte[] bytes;

            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                bytes = ms.ToArray();
            }

            return bytes;
        }
        #endregion
    }
}
