﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PawaoxUtility.Database
{
    public class PawSQLConnection
    {
        private readonly string connection;

        private SqlConnection con;

        public PawSQLConnection(string location, string database, string serverInstanceName)
        {
            connection = "Data Source=" + location + "\\" + serverInstanceName + ";";
            connection += "database=" + database + ";";
            connection += "Integrated Security=true;";

            con = new SqlConnection(connection);
        }

        public PawSQLConnection(string serverName, string database)
        {
            connection = "Data Source=" + serverName + ";";
            connection += "database=" + database + ";";
            connection += "Integrated Security=true;";

            con = new SqlConnection(connection);
        }

        public PawSQLConnection(string connectionString)
        {
            connection = connectionString;

            con = new SqlConnection(connection);
        }

        ~PawSQLConnection()
        {
            Close();
            con.Dispose();
        }

#region [WIP] Transactions

#warning TODO: Store pointers to transactions. In deconstructor, attempt to close them all in loop to avoid possible loss of data. Prevents the 'horrorstories' of losing a full days work.
#warning TODO: Methods in file close the connection, messing with transaction use. Create new file dedicated to transactions, method below temporary.

        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public SqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.Unspecified)
        {
            Open();
            return con.BeginTransaction(isolation);
        }

#endregion

        #region Query
        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public SqlDataReader Query(string sql)
        {
            return Query(CreateCommand(sql));
        }


        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public SqlDataReader Query(SqlCommand cmd)
        {
            Open();

            SqlException exception = null;
            SqlDataReader rdr = null;

            try { rdr = cmd.ExecuteReader(); }
            catch (SqlException e) { exception = e; }
            finally
            {
                if (exception != null)
                {
                    Close();
                    throw exception;
                }
            }

            return rdr;
        }

        public List<T> QuerySimple<T>(int columns, string sql)
        {
            SqlCommand cmd = CreateCommand(sql);

            Open();

            SqlException exception = null;
            SqlDataReader rdr = null;

            rdr = cmd.ExecuteReader();
            try { rdr = cmd.ExecuteReader(); }
            catch (SqlException e) { exception = e; }
            finally
            {
                if (exception != null)
                {
                    Close();
                    throw exception;
                }
            }

            List<T> list = new List<T>();

            while (rdr.Read())
                for (int i = 0; i < columns; i++)
                    list.Add((T)rdr[i]);

            Close();

            return list;
        }

        #endregion

        #region Selects

        public T SelectScalar<T>(string sql)
        {
            return SelectScalar<T>(CreateCommand(sql));
        }

        public T SelectScalar<T>(SqlCommand cmd)
        {
            Open();

            T t = (T)cmd.ExecuteScalar();

            Close();

            return t;
        }

        #endregion

        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public SqlDataReader StoredProcedureReader(string procedure, params object[] parameters)
        {
            SqlCommand cmd = CreateStoredProcedure(procedure, parameters);

            return Query(cmd);
        }

        public int PreparedStatementExecute(string sql, params object[] parameters)
        {
            return Execute(CreatePreparedStatement(sql, false, parameters));
        }

        public SqlCommand CreatePreparedStatement(string sql, bool procedure, params object[] parameters)
        {
            SqlCommand cmd = new SqlCommand(sql, con);

            if (procedure)
                cmd.CommandType = CommandType.StoredProcedure;

            SqlDbType type = SqlDbType.VarChar; // Non Nullable, must have a value :3
            int typeLength = 0; // Varchar length 1, cause why not?
            string vVariable = "";
            object vValue = null;

            bool alreadySet = false;
            SqlParameter sqlParam = null;

            int i = 0;
            while (i < parameters.Length)
            {
                if (parameters[i] is PawSqlStatementParam[])
                {
                    foreach (var param in parameters[i++] as PawSqlStatementParam[])
                    {
                        PawSqlStatementParam param1 = (PawSqlStatementParam)parameters[i++];
                        sqlParam = param1.SQLParam;
                        alreadySet = true;
                    }
                }
                else if (parameters[i] is PawSqlStatementParam)
                {
                    PawSqlStatementParam param2 = (PawSqlStatementParam)parameters[i++];
                    sqlParam = param2.SQLParam;
                    alreadySet = true;
                }
                else if (parameters[i] is SqlParameter)
                {
                    sqlParam = (SqlParameter)parameters[i++];
                    alreadySet = true;
                }
                else
                {
                    type = (SqlDbType)parameters[i++];

                    object next = parameters[i++];

                    if (next is int)
                    {
                        typeLength = (int)next;
                        vVariable = (string)parameters[i++];
                    }
                    else vVariable = (string)next;

                    vValue = parameters[i++];
                }

                if (!alreadySet)
                {
                    sqlParam = CreateSqlParameter(vVariable, type, typeLength);
                    sqlParam.Value = vValue;
                }
                else alreadySet = false;

                cmd.Parameters.Add(sqlParam);
            }

#warning TODO: Not needed in MsSQL?
            /*
            Open();
            //cmd.Prepare();
            Close();*/

            return cmd;
        }

        /// <summary>
        /// REMEMBER TO MANUALLY CLOSE THE CONNECTION AFTER USE!
        /// </summary>
        public SqlDataReader PreparedStatementQuery(string sql, params object[] parameters)
        {
            return Query(CreatePreparedStatement(sql, false, parameters));
        }

        public int PreparedStatement(string sql, params object[] parameters)
        {
            SqlCommand cmd = CreatePreparedStatement(sql, false, parameters);
            return Execute(cmd);
        }

        public T PreparedStatementScalar<T>(string sql, params object[] parameters)
        {
            SqlCommand cmd = CreatePreparedStatement(sql, false, parameters);
            return SelectScalar<T>(cmd);
        }

        public decimal? PreparedStatementGetIdentity(string sql, params object[] parameters)
        {
            SqlCommand cmd = CreatePreparedStatement(sql + "; SELECT Scope_Identity();", false, parameters);

            return SelectScalar<decimal?>(cmd);
        }

        public SqlCommand CreateStoredProcedure(string procedure, params object[] parameters)
        {
            return CreatePreparedStatement(procedure, true, parameters);
        }


        private SqlParameter CreateSqlParameter(string variable, SqlDbType type, int varCharLength = 0)
        {
            if (type == SqlDbType.VarChar)
                return new SqlParameter(variable, type, varCharLength);
            else return new SqlParameter(variable, type);
        }





        public void Open()
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
        }

        public void Close()
        {
            try
            {
                if (con.State == ConnectionState.Open && con.State != ConnectionState.Closed)
                    con.Close();
            }
                //It happened too fast, doesn't matter!
            catch (InvalidOperationException) { }
        }

        public int Execute(string sql)
        {
            return Execute(CreateCommand(sql));
        }

        public int Execute(SqlCommand cmd)
        {
            Open();
            int rows = cmd.ExecuteNonQuery();
            Close();

            return rows;
        }
        
        public SqlCommand CreateCommand(string sql)
        {
            return new SqlCommand(sql, con);
        }
    }
}
