﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawaoxUtility
{
    public static class PawDigits
    {
        #region Ensures

        public static string Ensure(int digits, string number)
        {
            while (number.Length < digits)
                number = "0" + number;

            return number;
        }

        #region Overloads

        public static string Ensure(int digits, int number)
        {
            return Ensure(digits, number.ToString());
        }

        public static string Ensure(int digits, double number)
        {
            return Ensure(digits, number.ToString());
        }

        #endregion

        #endregion
    }
}
