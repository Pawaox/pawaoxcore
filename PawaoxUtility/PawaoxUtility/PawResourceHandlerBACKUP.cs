﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PawaoxUtility
{
    public static class PawResourceHandlerBACKUP
    {
        public static Uri GetUriFromResource(string psAssemblyName, string psResourceName)
        {
            return new Uri("pack://application:,,,/" + psAssemblyName + ";component/" + psResourceName, UriKind.RelativeOrAbsolute);
        }
        
        public static Stream GetStreamFromResource(Assembly assembly, string path)
        {
            string assemblyName = assembly.FullName.Substring(0, assembly.FullName.IndexOf(",")).Replace(" ", "_");

            path = path.Replace('/', '.').Replace('\\', '.');

            if (!path.StartsWith("."))
                path = "." + path;
            
            return assembly.GetManifestResourceStream(assemblyName + path);
        }


        public static ImageSource GetImageSourceFromResource(string psAssemblyName, string psResourceName)
        {
            Uri oUri = GetUriFromResource(psAssemblyName, psResourceName);
            return BitmapFrame.Create(oUri, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
        }

        public static ImageSource GetImageSourceFromResource(Assembly assembly, string path)
        {
            Stream stream = GetStreamFromResource(assembly, path);
            return BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
        }

    }
}
