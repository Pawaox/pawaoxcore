﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PawaoxUtility
{
    public static class PawResourceHandler
    {
        /*
        public static Uri GetUriFromResource(string psAssemblyName, string psResourceName)
        {
            return new Uri("pack://application:,,,/" + psAssemblyName + ";component/" + psResourceName, UriKind.RelativeOrAbsolute);
        }
        public static Uri GetUriFromResource(Assembly assembly, string psResourceName)
        {
            return GetUriFromResource(GetAssemblyName(assembly), psResourceName);
        }*/
        
        
        public static Stream GetStreamFromResource(Assembly assembly, string path)
        {
            string assemblyName = assembly.FullName.Substring(0, assembly.FullName.IndexOf(",")).Replace(" ", "_");

            if (Char.IsDigit(assemblyName, 0))
                assemblyName = "_" + assemblyName;

            path = path.Replace('/', '.').Replace('\\', '.');

            if (!path.StartsWith("."))
                path = "." + path;

            return assembly.GetManifestResourceStream(assemblyName + path);
        }

        public static ImageSource GetImageSourceFromResource(Assembly assembly, string path)
        {
            Stream stream = GetStreamFromResource(assembly, path);
            return BitmapFrame.Create(stream);
        }

    }
}
